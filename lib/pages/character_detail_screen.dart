import 'package:after_layout/after_layout.dart';
import 'package:despicable_me/models/character.dart';
import 'package:despicable_me/styleguide.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class CharacterDetailScreen extends StatefulWidget {
  final Character character;
  final double _expandedBottomSheetBottomPosition = 0;
  final double _collapsedBottomSheetBottomPosition = -250;
  final double _completeBottomSheetBottomPosition = -330;

  const CharacterDetailScreen({Key key, this.character}) : super(key: key);

  @override
  _CharacterDetailScreenState createState() => _CharacterDetailScreenState();
}

class _CharacterDetailScreenState extends State<CharacterDetailScreen>
    with AfterLayoutMixin<CharacterDetailScreen> {
  double _bottomSheetBottomPosition = -330;
  bool isCollapsed = false;
  double rotateAngle = 0.5;

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        body: Stack(fit: StackFit.expand, children: <Widget>[
      Hero(
          tag: "background-${widget.character.name}",
          child: DecoratedBox(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: widget.character.colors,
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft)),
          )),
      SingleChildScrollView(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 40),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, left: 16.0),
            child: IconButton(
              icon: Icon(Icons.close,
                  color: Colors.white.withOpacity(0.89), size: 40),
              onPressed: () {
                setState(() {
                  _bottomSheetBottomPosition =
                      widget._completeBottomSheetBottomPosition;
                });
                Navigator.pop(context);
              },
            ),
          ),
          Align(
              alignment: Alignment.topRight,
              child: Hero(
                  tag: "image-${widget.character.name}",
                  child: Image.asset(
                    widget.character.imagePath,
                    height: 0.45 * screenHeight,
                  ))),
          Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 32.0, vertical: 8),
              child: Hero(
                tag: "name-${widget.character.name}",
                child: Container(
                    child: Material(
                        color: Colors.transparent,
                        child: Text(
                          widget.character.name,
                          style: AppTheme.heading,
                        ))),
              )),
          Padding(
              padding: const EdgeInsets.fromLTRB(32, 0, 8, 32),
              child: Text(
                widget.character.description,
                style: AppTheme.subHeading,
              ))
        ],
      )),
      AnimatedPositioned(
          duration: const Duration(milliseconds: 500),
          curve: Curves.decelerate,
          bottom: _bottomSheetBottomPosition,
          left: 0,
          right: 0,
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                InkWell(
                    onTap: _onTap,
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 32),
                      alignment: Alignment.centerLeft,
                      height: 80,
                      child: Row(children: <Widget>[
                        Text(
                          "Clips",
                          style:
                              AppTheme.subHeading.copyWith(color: Colors.black),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(105, 0, 0, 0),
                        ),
                        Transform.rotate(
                            angle: math.pi / rotateAngle,
                            child: Icon(
                              Icons.arrow_drop_up,
                              size: 40,
                            ))
                      ]),
                    )),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: _clipWidget(),
                )
              ],
            ),
          )),
    ]));
  }

  Widget _clipWidget() {
    return Container(
        height: 250,
        margin: const EdgeInsets.symmetric(horizontal: 16),
        child: Row(children: <Widget>[
          Column(children: <Widget>[
            roundContainer(Colors.redAccent),
            SizedBox(height: 20),
            roundContainer(Colors.greenAccent),
          ]),
          SizedBox(width: 16),
          Column(children: <Widget>[
            roundContainer(Colors.redAccent),
            SizedBox(height: 20),
            roundContainer(Colors.greenAccent),
          ]),
          SizedBox(width: 16),
          Column(children: <Widget>[
            roundContainer(Colors.redAccent),
            SizedBox(height: 20),
            roundContainer(Colors.greenAccent),
          ]),
          SizedBox(width: 16),
          Column(children: <Widget>[
            roundContainer(Colors.redAccent),
            SizedBox(height: 20),
            roundContainer(Colors.greenAccent),
          ]),
        ]));
  }

  Widget roundContainer(Color color) {
    return Container(
      width: 100,
      height: 100,
      decoration: BoxDecoration(
          color: color, borderRadius: BorderRadius.all(Radius.circular(20))),
    );
  }

  _onTap() {
    setState(() {
      _bottomSheetBottomPosition = isCollapsed
          ? widget._expandedBottomSheetBottomPosition
          : widget._collapsedBottomSheetBottomPosition;
      rotateAngle = isCollapsed ? 1.0 : 0.5;
      isCollapsed = !isCollapsed;
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    Future.delayed(const Duration(microseconds: 500), () {
      setState(() {
        isCollapsed = true;
        _bottomSheetBottomPosition = widget._collapsedBottomSheetBottomPosition;
      });
    });
  }
}
